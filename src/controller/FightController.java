package controller;


import java.net.URL;
/**
 * 
 * @author Berettini, Buzzolo, Ferrari, Lamonaca
 *
 */
public interface FightController {
	/**
	 * Open score machine JFrame, where the fight is ready to start
	 * @param cognome1 Surname of the first athlete
	 * @param cognome2 Surname of the second athlete
	 */
	public void startFight(String firstAthleteSurname, String secondAthleteSurname);
	/**
	 * Open the historical JFrame with all the fights fought
	 */
	public void printAthletes();
	/**
	 * Get  an Array of String containing the path of the blue points as JPG
	 * @param punti -actual blue points
	 * @return the paths of the JPG files
	 */
	public String[] getScoreBlue(Integer points);
	/**
	 * Get an Array of String containing the path of the red points as JPG
	 * @param punti
	 * @return the paths of the JPG
	 */
	public String[] getScoreRed(Integer points);
	/**
	 * Get an Array of String containing the path of the red warnings as JPG
	 * @param warnings
	 * @return the paths of the warnings
	 */
	public String getWarningRed(Integer warnings);
	/**
	 * Get an Array of String containing the path of the blue warnings as JPG
	 * @param warnings
	 * @return the paths of the warnings
	 */
	public String getWarningBlue(Integer warnings);
	/**
	 * 
	 * @param file
	 */
	public void playSound(URL file);
	/**
	 * 
	 */
	public void insertListaMatchFile();
	/**
	 * 
	 */
	public void getListaMatchFile();
	/**
	 * 
	 * @param atleta1
	 * @param atleta2
	 * @param risultato
	 */

	public void insertListaMatch(String firstAthleteSurname, String secondAthleteSurname, String result);
}
