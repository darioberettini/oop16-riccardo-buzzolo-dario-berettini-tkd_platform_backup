package controller;

import java.net.URL;

import model.Fight;
import view.FightPanel;
import view.FightStartedPanel;
import view.FightStartedPanelImpl;
import view.Historical;
import view.HistoricalImpl;

public class FightControllerImpl implements FightController {

	private Fight modelFight;
	private FightPanel fightPanel;

	public FightControllerImpl( Fight modelFight, FightPanel fightPanel){
		
		this.modelFight = modelFight;
		this.fightPanel = fightPanel;
		this.fightPanel.addObserver(this);
	}
	
	public void startFight(String firstAthleteSurname, String secondAthleteSurname){
		
		FightStartedPanel startedFight = new FightStartedPanelImpl(firstAthleteSurname,secondAthleteSurname);
		startedFight.addObserver(this);
		
	}
	
	public void printAthletes(){
		
		Historical historical = new HistoricalImpl();
		modelFight.setListMatch(modelFight.getListMatchFile());
		historical.StampaStoricoMatch(modelFight.getListMatch());
		historical.addObserverFight(this);
	}
	
	public String[] getScoreBlue(Integer points){
		
		return modelFight.getScoreBlue(points);
	}
	
	public String[] getScoreRed(Integer points){
		
		return modelFight.getScoreRed(points);
	}
	
	public String getWarningRed(Integer warnings){
		
		return modelFight.getWarningRed(warnings);
	}
	
	public String getWarningBlue(Integer warnings){
		
		return modelFight.getWarningBlue(warnings);
	}
	
	public void playSound(URL file){
		
		modelFight.playSound(file);
	}
	
	public void insertListaMatchFile(){
		
		modelFight.insertListMatchFile();
	}
	
	public void getListaMatchFile(){
		
		modelFight.getListMatchFile();
	}
	
	public void insertListaMatch(String firstAthleteSurname, String secondAthleteSurname, String result){
	
		modelFight.insertListMatch(firstAthleteSurname, secondAthleteSurname, result);
	}
}