package controller;

public interface MainController {

	boolean logIn(final String username, final String password);
	void examView();
	void fightView();
	void formView();
}