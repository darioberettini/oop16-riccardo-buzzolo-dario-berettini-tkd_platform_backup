package controller;

import java.util.ArrayList;
import java.util.List;
import model.Athlete;
import model.Belt;
import model.Form;
import view.FormPanel;
import view.FormStartedPanel;
import view.FormStartedPanelImpl;
import view.Historical;
import view.HistoricalImpl;

public class FormControllerImpl implements FormController {
	
	private Form modelForm;	
	private FormPanel formPanel;

	public FormControllerImpl( Form modelForm, FormPanel formPanel){
		
		this.modelForm = modelForm;
		this.formPanel = formPanel;
		this.formPanel.addObserver(this);
	}
	
	public void startForm(String athleteName, String athleteSurname, Belt athletBelt, String formName){
			
		FormStartedPanel formStarted = new FormStartedPanelImpl(athleteName, athleteSurname, athletBelt, formName);
		formStarted.addObserverForm(this);
	}
	
	public void printAthletes(){
			
		Historical historical = new HistoricalImpl();
		historical.StampaStoricoForma(getListAthleteFile());
		historical.addObserverForm(this);
	}
		
	public String[] getScoreRed(int points){
		
		return modelForm.getScoreRed(points);	
	}
		
	public List<Athlete> getListAthletesForm(){
			
		return modelForm.getListAthletesForm();
	}
	
	public void addAthlete(Athlete athlete){
			
		modelForm.addAhtletesForm(athlete);
	}
	
	public void addListAthleteFile(){
			
		modelForm.addListAthletesFormFile();
	}
	
	public ArrayList<Athlete> getListAthleteFile(){
		
		return modelForm.getListAthletesFormFile();
	}		
}
