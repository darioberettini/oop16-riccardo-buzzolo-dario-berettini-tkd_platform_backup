package view;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import controller.FightController;
import controller.FormController;
import model.Athlete;
import model.Match;

public class HistoricalImpl extends JFrame implements Historical{

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private FormController formController;
	@SuppressWarnings("unused")
	private FightController fightController;
	private JPanel mainPanel = new JPanel();
	private JLabel titleFormLabel = new JLabel("Storico Forme");
	private JLabel titleFightLabel = new JLabel("Storico Combattimenti");

	public HistoricalImpl(){
		
		super("Storico");
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 900, 700);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
		
	}
	public void createTable(final Object[][] data,final Object[] headers){

		DefaultTableModel tableModel;
		JTable historicalTable;
		JScrollPane historicalPane;
		
		tableModel = new DefaultTableModel(data,
				headers){	

			private static final long serialVersionUID = 2205643621514873449L;

			@Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		historicalTable = new JTable(tableModel);
		historicalPane = new JScrollPane(historicalTable);
		historicalTable.setFont(new Font("Arial", 0, 15));
		JTableHeader header = historicalTable.getTableHeader();
		header.setFont(new Font("Arial", Font.BOLD, 20));
		historicalPane.setBounds(0, 70 , 895, 595);
		mainPanel.add(historicalPane);
	}
	public void StampaStoricoForma(ArrayList<Athlete> listAthleteForm){
		
		titleFormLabel.setForeground(new Color(0, 0, 0));
		titleFormLabel.setFont(new Font("Arial", Font.BOLD, 25));
		titleFormLabel.setBounds(350, 15 , 400, 40);
		mainPanel.add(titleFormLabel);
		
		Object [][] athletes = new Object[listAthleteForm.size()][3];
		Object [] headers = new Object[]{"Nome","Cognome","Voto"};
		int index=0;
		for(Athlete athlete : listAthleteForm){
				System.out.println(athlete.getFormScore());
				String formScore = String.format("%.1f",athlete.getFormScore());
				athletes[index][0] = athlete.getName();
				athletes[index][1] = athlete.getSurname();
				athletes[index][2] = formScore;				
				index++;
		}
			
		createTable(athletes, headers);
	}

	@Override
	public void StampaStoricoMatch(ArrayList<Match> listaMatch) {
		titleFightLabel.setForeground(new Color(0, 0, 0));
		titleFightLabel.setFont(new Font("Arial", Font.BOLD, 25));
		titleFightLabel.setBounds(310, 15 , 400, 40);
		mainPanel.add(titleFightLabel);
		
		Object [][] matches = new Object[listaMatch.size()][3];
		Object [] headers = new Object[]{"Atleta1","Risultato","Atleta2"};
		int index=0;
		for(Match match : listaMatch){
				
			matches[index][0]=match.getFirstAthleteSurname();
			matches[index][1]=match.getResult();
			matches[index][2]=match.getSecondAthleteSurname();
			index++;
		}
		createTable(matches,headers);
		
	}
	
	public void addObserverForm(FormController controller) {
		this.formController = controller;
	}
	
	public void addObserverFight(FightController controller) {
		this.fightController = controller;
	}
	
}