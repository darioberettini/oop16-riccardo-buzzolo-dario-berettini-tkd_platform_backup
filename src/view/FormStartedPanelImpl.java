package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controller.FormController;
import model.Athlete;
import model.AthleteImpl;
import model.Belt;


public class FormStartedPanelImpl  extends JFrame implements FormStartedPanel{

	private static final long serialVersionUID = 3573309690612785259L;
	private FormController formController;
	private Athlete athlete;
	private JPanel mainPanel = new JPanel();
	private final JLabel decimalScoreLabel = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
	private final JLabel commaPoints = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/float_red.png")));
	private final JLabel unitScoreLabel = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
	private final JLabel dozenScoreLabel = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/1_red.png")));
	private final JLabel athleteName = new JLabel();
	private final JLabel athleteSurname = new JLabel("Default");
	private final Belt athleteBelt;
	private final JLabel beltLabel = new JLabel();
	private final JLabel formLabel = new JLabel();
	private final JLabel timeText = new JLabel("00:00");
	private final JButton penaltyOneButton = new JButton("-0,1");
	private final JButton penaltyThreeButton = new JButton("-0,3");
	private final JButton penaltyFiveButton = new JButton("-0,5");
	private final JButton startButton = new JButton("Start");
	private static Timer timer;
	private TimerTask timerTask;
	private Integer seconds = 0;
	private int puntiTotali = 100;
	private KeyEventDispatcher keyEventDispatcher;
	
	
	
	public FormStartedPanelImpl(String n, String co, Belt ci, String f){
		
		this.athleteBelt = ci;
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 1200, 700);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
		mainPanel.setBackground(new Color(0, 0, 0));
		athleteName.setText(n);
		
		athleteSurname.setText(co);
		athleteSurname.setForeground(new Color(255, 255, 255));
		athleteSurname.setFont(new Font("Arial", Font.BOLD, 30));
		athleteSurname.setBounds(20, 20, 300, 60);
		athleteSurname.setOpaque(true);
		athleteSurname.setBackground(new Color(255, 0, 0));
		mainPanel.add(athleteSurname);
		
		formLabel.setText("   " + f);
		formLabel.setForeground(new Color(255, 255, 255));
		formLabel.setFont(new Font("Arial", Font.BOLD, 18));
		formLabel.setBounds(20, 90, 300, 40);
		formLabel.setOpaque(true);
		formLabel.setBackground(new Color(255, 0, 0));
		mainPanel.add(formLabel);
		
		beltLabel.setText("                  " + athleteBelt);
		beltLabel.setForeground(new Color(255, 255, 255));
		beltLabel.setFont(new Font("Arial", Font.BOLD, 20));
		beltLabel.setBounds(20, 135, 300, 40);
		beltLabel.setOpaque(true);
		beltLabel.setBackground(new Color(255, 0, 0));
		mainPanel.add(beltLabel);
		
		penaltyOneButton.setForeground(new Color(0, 0, 0));
		penaltyOneButton.setFont(new Font("Arial", Font.BOLD, 20));
		penaltyOneButton.setBounds(177, 550 , 150, 50);
		mainPanel.add(penaltyOneButton);
		penaltyOneButton.setEnabled(false);
		
		penaltyThreeButton.setForeground(new Color(0, 0, 0));
		penaltyThreeButton.setFont(new Font("Arial", Font.BOLD, 20));
		penaltyThreeButton.setBounds(512, 550 , 150, 50);
		mainPanel.add(penaltyThreeButton);
		penaltyThreeButton.setEnabled(false);
		
		penaltyFiveButton.setForeground(new Color(0, 0, 0));
		penaltyFiveButton.setFont(new Font("Arial", Font.BOLD, 20));
		penaltyFiveButton.setBounds(842, 550 , 150, 50);
		mainPanel.add(penaltyFiveButton);
		penaltyFiveButton.setEnabled(false);
		
		startButton.setForeground(new Color(0, 0, 0));
		startButton.setFont(new Font("Arial", Font.BOLD, 20));
		startButton.setBounds(970, 20 , 200, 50);
		mainPanel.add(startButton);
				
		timeText.setFont(new Font("Arial", Font.BOLD, 50));
		timeText.setForeground(new Color(255, 255, 255));
		timeText.setBounds(530, 20 , 150, 50);
		mainPanel.add(timeText);
		
		dozenScoreLabel.setBounds(340, 100 , 200, 420);
		mainPanel.add(dozenScoreLabel);
		
		unitScoreLabel.setBounds(540, 100 , 200, 420);
		mainPanel.add(unitScoreLabel);
		
		commaPoints.setBounds(740, 100 , 50, 420);
		mainPanel.add(commaPoints);
		
		decimalScoreLabel.setBounds(790, 100 , 200, 420);
		mainPanel.add(decimalScoreLabel);

		timer = new Timer();
		
		startButton.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
			
				if(startButton.getText()=="Start"){
					startButton.setText("Stop");
					penaltyOneButton.setEnabled(true);
					penaltyThreeButton.setEnabled(true);
					penaltyFiveButton.setEnabled(true);
					timerTask = new TimerTask() {
						
						@Override
						public void run() {
							if(seconds>58){
								seconds=seconds-60;
								if(seconds<11)
									timeText.setText("01:0" + ++seconds+"");
								else
									timeText.setText("01:" + ++seconds+"");
								seconds=seconds+60;
							}
							else if(seconds < 9){
									timeText.setText("00:0" + ++seconds+"");
							}else {
									timeText.setText("00:" + ++seconds+"");
							}
						}
					};
					
					timer.scheduleAtFixedRate(timerTask, 0, 1000);
					
				} else if(startButton.getText()=="Stop"){
						startButton.setText("Salva e chiudi");
						penaltyOneButton.setEnabled(false);
						penaltyThreeButton.setEnabled(false);
						penaltyFiveButton.setEnabled(false);
						timer.cancel();
					
				}else {
		
					athlete = new AthleteImpl(athleteName.getText(), athleteSurname.getText(), athleteBelt, formLabel.getText(),(double)puntiTotali/10);
					System.out.println(athleteName.getText()+"-");
					formController.addAthlete(athlete);
					formController.addListAthleteFile();	
					KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);	
					dispose();
					
				}
			}
		});
		
		penaltyOneButton.addActionListener(e->{
			
				if(puntiTotali-1<=0){
					puntiTotali=0;
					JFrame frame = new JFrame("Errore");			   
				    JOptionPane.showMessageDialog(frame,
				      "Prova terminata. Voto minimo.");	
				    timer.cancel();
				    athlete = new AthleteImpl(athleteName.getText(), athleteSurname.getText(), athleteBelt, formLabel.getText(), (double)puntiTotali/10);
				 	formController.addAthlete(athlete);
					formController.addListAthleteFile();	
					dispose();
					
				}else{
					puntiTotali = puntiTotali-1;
					dozenScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[0])));
					unitScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[1])));
					decimalScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[2])));
				}
			}
		);
		
		penaltyThreeButton.addActionListener(e->{
				if(puntiTotali-3<=0){
					puntiTotali=0;
					JFrame frame = new JFrame("Errore");			   
				    JOptionPane.showMessageDialog(frame,
				      "Prova terminata. Voto minimo.");	
				    timer.cancel();
				    athlete = new AthleteImpl(athleteName.getText(), athleteSurname.getText(), athleteBelt, formLabel.getText(), (double)puntiTotali/10);
				 	formController.addAthlete(athlete);
					formController.addListAthleteFile();		
					dispose();
					
				}else{
					puntiTotali = puntiTotali-3;
					dozenScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[0])));
					unitScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[1])));
					decimalScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[2])));
				}
		});
		
		penaltyFiveButton.addActionListener(e->{
				if(puntiTotali-5<=0){
					puntiTotali=0;
					JFrame frame = new JFrame("Errore");			   
				    JOptionPane.showMessageDialog(frame,
				      "Prova terminata. Voto minimo.");	
				    timer.cancel();
				    athlete = new AthleteImpl(athleteName.getText(), athleteSurname.getText(), athleteBelt, formLabel.getText(),(double) puntiTotali/10);
				 	formController.addAthlete(athlete);
					formController.addListAthleteFile();		
					dispose();
					
				}else{
					puntiTotali = puntiTotali-5;
					dozenScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[0])));
					unitScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[1])));
					decimalScoreLabel.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.formController.getScoreRed(puntiTotali)[2])));
				}
		});
		 keyEventDispatcher = new KeyEventDispatcher(){
			public boolean dispatchKeyEvent(KeyEvent e) 
        {
            if (e.getID()== KeyEvent.KEY_TYPED)
            {
                String key = "" + e.getKeyChar();
                
                if (key.equalsIgnoreCase("T"))
                {
                    startButton.doClick();
                } else if (key.equalsIgnoreCase("1"))
                {
                    penaltyOneButton.doClick();
                }else if (key.equalsIgnoreCase("3"))
                {
                    penaltyThreeButton.doClick();
                }else if (key.equalsIgnoreCase("5"))
                {
                    penaltyFiveButton.doClick();
                }
                
             }
			return rootPaneCheckingEnabled;
         }
    };
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
		
		this.setFocusable(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {

			  @Override
			  public void windowClosing(WindowEvent we)
			  { 
			    String ObjButtons[] = {"Si","No"};
			    int PromptResult = JOptionPane.showOptionDialog(null, 
			        "Lo stato dell'esecuzione della forma verrà perso, sei sicuro di vole uscire?", "Attenzione", 
			        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, 
			        ObjButtons,ObjButtons[1]);
			    if(PromptResult==0)
			    {
			        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);	
			    	dispose();            
			    }
			  }
		});
	}

	public void addObserverForm(FormController controller) {
		this.formController = controller;
	}
}
