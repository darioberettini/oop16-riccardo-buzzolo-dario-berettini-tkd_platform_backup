package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import controller.MainController;

public class MainPanelImpl extends JFrame implements MainPanel{

	private static final long serialVersionUID = 8607610145685675869L;
	private static final String DB_URL = "jdbc:mysql://localhost:3306/credenzialiutenti";
   private static final String DB_USERNAME = "root";
   private static final String DB_PASSWORD = ".gimbo4e";
	private JPanel mainPanel = new JPanel();
	private JPanel logInPanel = new JPanel(); 
	private final JLabel titleLoginLabel = new JLabel("Benvenuto nella piattaforma Taekwondo");
	private final JLabel loginLabel = new JLabel("Inserire dati");
	private final JLabel usernameLabel = new JLabel("Nome utente:");
	private final JTextField usernameText = new JTextField(20);
	private final JLabel passwrdLabel = new JLabel("Password:");
	private final JPasswordField passwordText = new JPasswordField(20);
	private final JButton submitButton = new JButton("Login");
	private JButton reset = new JButton("Reset");
	private MainController mainController;
	private final JButton fightButton = new JButton("Combattimento");
	private final JButton formButton = new JButton("Forma");
	private final JButton examButton  = new JButton("Esame");
	private URL url = MainPanelImpl.class.getResource("/image.jpg");
	private final JLabel backgroundLabel = new JLabel(new ImageIcon(url));
	/*private Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
	private Double width = screensize.getWidth();
	private Double height = screensize.getHeight();*/

	public MainPanelImpl(){
		
		super("Taekwondo Platform");
	
	 // LOGIN
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 800, 480);
		logInPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(logInPanel);
		logInPanel.setLayout(null);
		
		titleLoginLabel.setForeground(new Color(0, 0, 0));
		titleLoginLabel.setBounds(100, 30, 600, 40);
		titleLoginLabel.setFont(new Font("Arial", Font.BOLD, 30));
		titleLoginLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		logInPanel.add(titleLoginLabel);
		
		loginLabel.setForeground(new Color(0, 0, 0));
		loginLabel.setBounds(340, 80, 100, 40);
		loginLabel.setFont(new Font("Arial", Font.BOLD, 16));
		loginLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		logInPanel.add(loginLabel);
		
		usernameLabel.setForeground(new Color(0, 0, 0));
		usernameLabel.setBounds(100, 150, 100, 40);
		usernameLabel.setFont(new Font("Arial", Font.BOLD, 15));
		// name.setBackground(new Color(255, 255, 255));
		logInPanel.add(usernameLabel);
		
		usernameText.setForeground(new Color(0, 0, 0));
		usernameText.setBounds(250, 150, 450, 40);
		usernameText.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		logInPanel.add(usernameText);
		
		passwrdLabel.setForeground(new Color(0, 0, 0));
		passwrdLabel.setBounds(100, 200, 100, 40);
		passwrdLabel.setFont(new Font("Arial", Font.BOLD, 15));
		// name.setBackground(new Color(255, 255, 255));
		logInPanel.add(passwrdLabel);
		
		passwordText.setForeground(new Color(0, 0, 0));
		passwordText.setBounds(250, 200, 450, 40);
		passwordText.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		logInPanel.add(passwordText);
		
		submitButton.setForeground(new Color(0, 0, 0));
		submitButton.setFont(new Font("Arial", Font.BOLD, 13));
		submitButton.setBounds(350, 350 , 120, 50);
		logInPanel.add(submitButton);
		
		reset.setBounds(50, 350 , 120, 50);
		logInPanel.add(reset);
		
	 // MAIN
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	    this.setResizable(false);
	    this.setBounds(100, 100, 800, 480);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mainPanel.setLayout(null);
		
		mainPanel.setBackground(new Color(250, 250, 250));
		
		backgroundLabel .setBounds(200, 0, 420, 150);
		backgroundLabel .setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(backgroundLabel );
		
		fightButton.setForeground(new Color(0, 0, 0));
		fightButton.setOpaque(true);
		fightButton.setFont(new Font("Arial", Font.BOLD, 25));
		fightButton.setBounds(0, 150 , 799, 100);
		mainPanel.add(fightButton);
		
		formButton.setForeground(new Color(0, 0, 0));
		formButton.setOpaque(true);
		formButton.setFont(new Font("Arial", Font.BOLD, 25));
		formButton.setBounds(0, 250 , 799, 100);
		mainPanel.add(formButton);
		
		examButton.setForeground(new Color(0, 0, 0));
		examButton.setOpaque(true);
		examButton.setFont(new Font("Arial", Font.BOLD, 25));
		examButton.setBounds(0, 350 , 799, 100);
		mainPanel.add(examButton);
		
		fightButton.addActionListener(e -> {
			
			mainController.fightView();
			this.toBack();
		});
		
		formButton.addActionListener(e -> {

			mainController.formView();
			this.toBack();
		});
		
		examButton.addActionListener(e -> {
			
			mainController.examView();
			this.toBack();
		});
		
		//ActionListener dell'OK per il login
		submitButton.addActionListener(e -> {
			 if(usernameText.getText().length() == 0){
			    JOptionPane.showMessageDialog(null, "Inserire username.");
			 } else if(passwordText.getPassword().length == 0) {
			    JOptionPane.showMessageDialog(null, "Inserire password.");
			 } else {
			    final String un = usernameText.getText();
			    final char[] pss = passwordText.getPassword();
			    final String pw = String.copyValueOf(pss);
			    if(validateLogin(un, pw)) {
			   	 logInPanel.setVisible(false);
			       this.setContentPane(mainPanel);
			    } else {
			       JOptionPane.showMessageDialog(null, "Accesso negato.");
			    }
			 }
		});
		
		reset.addActionListener(e -> {
			reset();
		});
		
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {

			  @Override
			  public void windowClosing(WindowEvent we)
			  { 
			    String ObjButtons[] = {"Si","No"};
			    int PromptResult = JOptionPane.showOptionDialog(null, 
			        "Sei sicuro di vole uscire?", "Attenzione", 
			        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, 
			        ObjButtons,ObjButtons[1]);
			    if(PromptResult==0)
			    {
			      System.exit(0);          
			    }
			  }
		});
		this.setLocationRelativeTo(null);	
	}
	
	private boolean validateLogin(final String username, final String password) {
      try {          
	 Connection conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);  
	 PreparedStatement pst = conn.prepareStatement("SELECT * FROM credenzialiutenti.account");
	 ResultSet rs = pst.executeQuery();                        
	 
	 while(rs.next()) {
	    if(username.equals(rs.getString("username")) && password.equals(rs.getString("password"))) {
	       conn.close();
	       return true;
	    }
	 }
	 
	 conn.close();
	 return false;
      } catch(Exception e) {
	 e.printStackTrace();
	 return false;
      }
   }
   
   private void reset() {
      usernameText.setText(null);
      passwordText.setText(null);
   }
	
	public void addObserver(MainController controller) {

		this.mainController = controller;
	}
}
