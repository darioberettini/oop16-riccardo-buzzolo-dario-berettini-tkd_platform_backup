package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import controller.FightController;


public class FightStartedPanelImpl extends JFrame implements FightStartedPanel{

	private static final long serialVersionUID = 1L;
	private FightController fightController;
	private static Timer timerFight;
	private static Timer timerPausa ;
	private JPanel mainPanel = new JPanel();	
	private final JButton startButton = new JButton("Start");
	private final JButton pauseButton = new JButton("Pause");
	private final JLabel instructionsLabel = new JLabel("?");
	private final JButton undoButton = new JButton("Undo");
	private final JButton onePointBlueButton = new JButton("+1");
	private final JButton onePointRedButton = new JButton("+1");
	private final JButton threePointBlueButton = new JButton("+3");
	private final JButton threePointRedButton = new JButton("+3");
	private final JButton warningOneRedButton = new JButton("X");
	private final JLabel[] warningRed = new JLabel[10];
	private final JButton warningOneBlueButton = new JButton("X");
	private final JButton koBlueButton = new JButton("KO");
	private final JButton koRedButton = new JButton("KO");
	private final JLabel[] warningBlue = new JLabel[10];
	private final JLabel secondsFightText = new JLabel("01:00");
	private final JLabel secondsPauseText = new JLabel("Break: 00:15");
	private final JLabel roundLabel = new JLabel("    R - ");
	private final JLabel unitBluePointsImage = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_blue.png")));
	private final JLabel dozenBluePointsImage = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_blue.png")));
	private final JLabel dozenRedPointsImage = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
	private final JLabel unitRedPointsImage = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
	private final JLabel surnameFirstAthleteLabel = new JLabel();
	private final JLabel surnameSecondAthleteLabel = new JLabel();
	private Integer bluePoints=0;
	private Integer redPoints=0;
	
	private Integer redWarnings=0;
	private Integer blueWarnings=0;

	private Integer secondsFight;
	private Integer currentRound;
	
	private String surnameFirstAthlete;
	private String surnameSecondAthlete;
	private String result;
	private int xCoordinateBlue = 1125;
	private int xCoordinateRed = 10;


	private String beepFileAudio1 = "/audio/beep1.wav";
	private String beepFileAudio2 = "/audio/beep2.wav";
	
	private List<Integer> operationDoneList = new ArrayList<>();
	
	private TimerTask timerTaskFight;
	
	private KeyEventDispatcher keyEventDispatcher;
	
	public FightStartedPanelImpl(final String cognome1, final String cognome2){	
				
		this.surnameFirstAthlete = cognome1;
		this.surnameSecondAthlete = cognome2;
		this.surnameFirstAthleteLabel.setText("                        " + this.surnameFirstAthlete);
		this.surnameSecondAthleteLabel.setText("                     " + this.surnameSecondAthlete);
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 1200, 700);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
		mainPanel.setBackground(new Color(0, 0, 0));
		
		startButton.setForeground(new Color(0, 0, 0));
		startButton.setFont(new Font("Arial", Font.BOLD, 20));
		startButton.setBounds(10, 10 , 150, 50);
		mainPanel.add(startButton);
		
		pauseButton.setForeground(new Color(0, 0, 0));
		pauseButton.setFont(new Font("Arial", Font.BOLD, 20));
		pauseButton.setBounds(163, 10 , 150, 50);
		mainPanel.add(pauseButton);
		
		instructionsLabel.setForeground(new Color(255,255,255));
		instructionsLabel.setFont(new Font("Arial", Font.BOLD, 20));
		instructionsLabel.setBounds(330, 10 , 20, 50);
		instructionsLabel.setOpaque(true);
		instructionsLabel.setBackground(new Color(0, 0, 0));
		mainPanel.add(instructionsLabel);
		
		roundLabel.setFont(new Font("Arial", Font.BOLD, 20));
		roundLabel.setBackground(new Color(255, 165, 0));
		roundLabel.setForeground(new Color(0, 0, 0));
		roundLabel.setOpaque(true);
		roundLabel.setBounds(545, 520, 100, 100);
		mainPanel.add(roundLabel);
		
		undoButton.setForeground(new Color(0, 0, 0));
		undoButton.setFont(new Font("Arial", Font.BOLD, 20));
		undoButton.setBounds(1035, 10 , 150, 50);
		mainPanel.add(undoButton);	
		
		surnameFirstAthleteLabel.setForeground(new Color(255, 255, 255));
		surnameFirstAthleteLabel.setFont(new Font("Arial", Font.BOLD, 30));
		surnameFirstAthleteLabel.setBounds(10, 65, 500, 40);
		surnameFirstAthleteLabel.setOpaque(true);
		surnameFirstAthleteLabel.setBackground(new Color(255, 0, 0));
		mainPanel.add(surnameFirstAthleteLabel);
		
		surnameSecondAthleteLabel.setForeground(new Color(255, 255, 255));
		surnameSecondAthleteLabel.setFont(new Font("Arial", Font.BOLD, 30));
		surnameSecondAthleteLabel.setBounds(700, 65, 485, 40);
		surnameSecondAthleteLabel.setOpaque(true);
		surnameSecondAthleteLabel.setBackground(new Color(0, 0, 255));
		mainPanel.add(surnameSecondAthleteLabel);
		
		secondsFightText.setFont(new Font("Arial", Font.BOLD, 50));
		secondsFightText.setForeground(new Color(255, 255, 255));
		secondsFightText.setBounds(530, 10 , 150, 50);
		mainPanel.add(secondsFightText);
		
		secondsPauseText.setFont(new Font("Arial", Font.BOLD, 30));
		secondsPauseText.setForeground(new Color(255, 255, 255));
		secondsPauseText.setBounds(515, 60 , 200, 50);
		mainPanel.add(secondsPauseText);
		
		unitRedPointsImage.setBounds(390, 100 , 200, 420);
		mainPanel.add(unitRedPointsImage);
		dozenRedPointsImage.setBounds(190, 100 , 200, 420);
		mainPanel.add(dozenRedPointsImage);
		
		unitBluePointsImage.setBounds(800, 100 , 200, 420);
		mainPanel.add(unitBluePointsImage);
		dozenBluePointsImage.setBounds(600, 100 , 200, 420);
		mainPanel.add(dozenBluePointsImage);
		
		koRedButton.setForeground(new Color(0, 0, 0));
		koRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		koRedButton.setBounds(90, 520 , 100, 60);
		mainPanel.add(koRedButton);
		
		onePointRedButton.setForeground(new Color(0, 0, 0));
		onePointRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		onePointRedButton.setBounds(190, 520 , 100, 60);
		mainPanel.add(onePointRedButton);
		
		threePointRedButton.setForeground(new Color(0, 0, 0));
		threePointRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		threePointRedButton.setBounds(290, 520 , 100, 60);
		mainPanel.add(threePointRedButton);
		
		warningOneBlueButton.setForeground(new Color(0, 0, 0));
		warningOneBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		warningOneBlueButton.setBounds(700, 520 , 100, 60);
		mainPanel.add(warningOneBlueButton);
		
		threePointBlueButton.setForeground(new Color(0, 0, 0));
		threePointBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		threePointBlueButton.setBounds(800, 520 , 100, 60);
		mainPanel.add(threePointBlueButton);
		
		onePointBlueButton.setForeground(new Color(0, 0, 0));
		onePointBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		onePointBlueButton.setBounds(900, 520 , 100, 60);
		mainPanel.add(onePointBlueButton);
		
		koBlueButton.setForeground(new Color(0, 0, 0));
		koBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		koBlueButton.setBounds(1000, 520 , 100, 60);
		mainPanel.add(koBlueButton);
		
		warningOneRedButton.setForeground(new Color(0, 0, 0));
		warningOneRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		warningOneRedButton.setBounds(390, 520 , 100, 60);
		mainPanel.add(warningOneRedButton);
						
		this.pauseButton.setEnabled(false);
		this.onePointBlueButton.setEnabled(false);
		this.onePointRedButton.setEnabled(false);
		this.threePointBlueButton.setEnabled(false);
		this.threePointRedButton.setEnabled(false);
		this.koBlueButton.setEnabled(false);
		this.koRedButton.setEnabled(false);
		this.warningOneBlueButton.setEnabled(false);
		this.warningOneRedButton.setEnabled(false);
		this.instructionsLabel.addMouseListener(new LabelAdapter());
		this.secondsFight = 61;
		this.currentRound = 0;
		

	    
		startButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				fightController.playSound(FightStartedPanelImpl.class.getResource(beepFileAudio1));	
				secondsPauseText.setText("Break: 00:15");
				timerFight = new Timer();
				timerTaskFight = new TimerTask(){
					Integer seconds = secondsFight;
					@Override
					public void run() {
						if(seconds>60){
							seconds=seconds-60;
							if(seconds<11)
								secondsFightText.setText("01:0"+--seconds);
							else
								secondsFightText.setText("01:"+--seconds);			
							seconds=seconds+60;
						}else if(seconds<11){
							secondsFightText.setText("00:0"+--seconds);
						}else{
							secondsFightText.setText("00:"+--seconds);
						}
						if(seconds<=5 && seconds>=1){
							fightController.playSound(FightStartedPanelImpl.class.getResource(beepFileAudio2));
						}
						if(currentRound==3){
													
							if(bluePoints > redPoints+11){
								result = cognome2+" vince per differenza punti. "+ bluePoints+"-"+redPoints;
								endFightMessage(cognome2+" vince per differenza punti.");
							    
							}else if(bluePoints+11  < redPoints){
								
								result = cognome1+" vince per differenza punti. "+ bluePoints+"-"+redPoints;
							    endFightMessage(cognome1+" vince per differenza punti.");
							}
						}
						if(currentRound==3 && seconds==0){
							if(bluePoints > redPoints){
								result= cognome2+" vince con un punteggio di "+ bluePoints+"-"+redPoints;
								endFightMessage(cognome2+" vince con un punteggio di "+ bluePoints+"-"+redPoints);
							}else if(bluePoints < redPoints){
								result=cognome1+" vince con un punteggio di "+ redPoints+"-"+bluePoints;
								endFightMessage(cognome1+" vince con un punteggio di "+ redPoints+"-"+bluePoints);
							}else if(bluePoints == redPoints){
								result="Pareggio";
								endFightMessage("Pareggio");
							}
						}
						if(seconds==0){
							
							if(bluePoints > redPoints+11 && currentRound==2){
								result = cognome2+" vince per differenza punti. "+ bluePoints+"-"+redPoints;
								endFightMessage(cognome2+" vince per differenza punti.");
							    
							}else if(bluePoints+11  < redPoints && currentRound==2){
								
								result = cognome1+" vince per differenza punti. "+ bluePoints+"-"+redPoints;
							    endFightMessage(cognome1+" vince per differenza punti.");

							}
							fightController.playSound(FightStartedPanelImpl.class.getResource(beepFileAudio1));	
							timerPausa = new Timer();
							TimerTask timerTaskPausa = new TimerTask(){
								
								Integer secondiPausa=15;
								
								@Override
								public void run() {
									
										if(secondiPausa > 0){
											if(secondiPausa<10)
												secondsPauseText.setText("Break: 00:0" + secondiPausa);	
											else
												secondsPauseText.setText("Break: 00:" + secondiPausa);		
											secondiPausa--;	
											startButton.setEnabled(false);
											pauseButton.setEnabled(false);
											onePointBlueButton.setEnabled(false);
											onePointRedButton.setEnabled(false);
											warningOneBlueButton.setEnabled(false);
											warningOneRedButton.setEnabled(false);
											threePointBlueButton.setEnabled(false);
											threePointRedButton.setEnabled(false);
											koBlueButton.setEnabled(false);
											koRedButton.setEnabled(false);
										}else {
											secondsPauseText.setText("Break: 00:0" + secondiPausa);		
											startButton.setEnabled(true);
											startButton.setText("Start");
											pauseButton.setEnabled(false);												
											this.cancel();
										}
									}
							};
							timerPausa.scheduleAtFixedRate(timerTaskPausa, 0, 1000);
							seconds = 6;	
							roundLabel.setText("    R - "+ ++currentRound);
							this.cancel();
							
						}
						
						secondsFight = seconds;					
					}
				};		
				
				timerFight.scheduleAtFixedRate(timerTaskFight, 0, 1000);
				
				startButton.setEnabled(false);
				pauseButton.setEnabled(true);	
				startButton.setText("Resume");
				warningOneBlueButton.setEnabled(true);
				warningOneRedButton.setEnabled(true);
				onePointBlueButton.setEnabled(true);
				onePointRedButton.setEnabled(true);
				threePointBlueButton.setEnabled(true);
				threePointRedButton.setEnabled(true);
				koBlueButton.setEnabled(true);
				koRedButton.setEnabled(true);
			}
		});
		
		pauseButton.addActionListener(e->{
			
			pauseButton.setEnabled(false);			
			startButton.setEnabled(true);
			timerFight.cancel();
		});	
		
		onePointBlueButton.addActionListener(e->{
			
			bluePoints++;
			if(bluePoints<99){
				if(bluePoints<10){
					unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
				}else{
					dozenBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
					unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[1])));
	
				}
				operationDoneList.add(0);
			}
		});
		
		onePointRedButton.addActionListener(e->{
			
			redPoints++;
			if(redPoints<99){
				if(redPoints<10){
					unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
				}else{
					dozenRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
					unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[1])));
					
				}
				operationDoneList.add(1);
			}
		});
		
		threePointBlueButton.addActionListener(e->{
			bluePoints+=3;
			if(bluePoints<99){
				if(bluePoints<10){
					unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
				}else{
					dozenBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
					unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[1])));
	
				}
				
				operationDoneList.add(2);	
			}
		});	
		
		threePointRedButton.addActionListener(e->{
			
			redPoints+=3;
			if(redPoints<99){
				if(redPoints<10){
					unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
				}else{
					dozenRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
					unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[1])));
					
				}
				operationDoneList.add(3);
			}
		});
		
		warningOneRedButton.addActionListener(e->{
						
			if(redWarnings<10){
				redWarnings++;
				 if(redWarnings==10){
					 
					 	result = cognome1+" 10 warning. "+cognome2+" vince.";
						endFightMessage(cognome1+" 10 warning."+cognome2+" vince");
						

				 }
				if(redWarnings%2==0){
					mainPanel.remove(warningRed[redWarnings-1]);
					warningRed[redWarnings-1] = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource(fightController.getWarningRed(2))));
					warningRed[redWarnings-1].setBounds(xCoordinateRed, 570, 60, 110);
					mainPanel.add(warningRed[redWarnings-1]);
					xCoordinateRed = xCoordinateRed + 63;
				}else {	
					warningRed[redWarnings] = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource(fightController.getWarningRed(1))));
					warningRed[redWarnings].setBounds(xCoordinateRed, 570, 60, 110);
					mainPanel.add(warningRed[redWarnings]);	
					
				}
				SwingUtilities.updateComponentTreeUI(this);	
				operationDoneList.add(4);
			}

		});
		
		warningOneBlueButton.addActionListener(e->{
			
			if(blueWarnings<10){
				blueWarnings++;
				if(blueWarnings==10){
					result = cognome2+" 10 warning."+cognome1+" vince. ";

					endFightMessage(cognome2+" 10 warning."+cognome1+" vince");
					
				}
				if(blueWarnings%2==0){	
					mainPanel.remove(warningBlue[blueWarnings-1]);
					warningBlue[blueWarnings-1] = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource(fightController.getWarningBlue(2))));
					warningBlue[blueWarnings-1].setBounds(xCoordinateBlue, 570, 60, 110);
					mainPanel.add(warningBlue[blueWarnings-1]);	
					xCoordinateBlue = xCoordinateBlue - 63;
				}else{
					warningBlue[blueWarnings] = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource(fightController.getWarningBlue(1))));
					mainPanel.add(warningBlue[blueWarnings]);	
					warningBlue[blueWarnings].setBounds(xCoordinateBlue, 570, 60, 110);
					
				}
				SwingUtilities.updateComponentTreeUI(this);	
				operationDoneList.add(5);
			}

			
		});
		koBlueButton.addActionListener(e->{
			
			result =cognome2+" vince per K.O";
			endFightMessage(cognome2+" vince per K.O");
			
		});
		
		koRedButton.addActionListener(e->{
			result = cognome1+" vince per K.O";
			endFightMessage(cognome1+" vince per K.O");
			
		});
		undoButton.addActionListener(e->{
			if(operationDoneList.isEmpty() == false){
				if(operationDoneList.get(operationDoneList.size()-1) == 0){				
					bluePoints--;
					if(bluePoints<10){
						dozenBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_blue.png")));
						unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
					}else{
						dozenBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
						unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[1])));
					}
				}
				if(operationDoneList.get(operationDoneList.size()-1)==1){				
					redPoints--;
					if(redPoints<10){
						dozenRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
						unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
					}else{
						dozenRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
						unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[1])));
						
					}
				}
				if(operationDoneList.get(operationDoneList.size()-1)==2){
					bluePoints=bluePoints-3;
					if(bluePoints<10){
						dozenBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_blue.png")));
						unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
					}else{
						dozenBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[0])));
						unitBluePointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreBlue(bluePoints)[1])));

					}
				}
				if(operationDoneList.get(operationDoneList.size()-1)==3){
					redPoints=redPoints-3;
					if(redPoints<10){
						dozenRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
						unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
					}else{
						dozenRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[0])));
						unitRedPointsImage.setIcon(new ImageIcon(FightStartedPanelImpl.class.getResource(this.fightController.getScoreRed(redPoints)[1])));
					}
				}
				if(operationDoneList.get(operationDoneList.size()-1)==4){
				
						 if(redWarnings%2==0){
							mainPanel.remove(warningRed[redWarnings-1]);
							warningRed[redWarnings-1] = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource(fightController.getWarningRed(1))));
							mainPanel.add(warningRed[redWarnings-1]);
							xCoordinateRed = xCoordinateRed - 63;
						}else {			
							mainPanel.remove(warningRed[(redWarnings)]);
							
						}	
						redWarnings--;
				}
				if(operationDoneList.get(operationDoneList.size()-1)==5){
					
						if(blueWarnings%2==0){	
							mainPanel.remove(warningBlue[blueWarnings-1]);
							warningBlue[blueWarnings-1] = new JLabel(new ImageIcon(FightStartedPanelImpl.class.getResource(fightController.getWarningBlue(1))));
							mainPanel.add(warningBlue[blueWarnings-1]);	
							xCoordinateBlue = xCoordinateBlue - 63;
			
						}else{
							mainPanel.remove(warningBlue[blueWarnings]);	
							
						}
						blueWarnings--;
				}
				operationDoneList.remove(operationDoneList.size()-1);
			
				SwingUtilities.updateComponentTreeUI(this);	
			}
		});
		
		roundLabel.setText("    R - "+ ++currentRound);
		this.keyEventDispatcher =   new KeyEventDispatcher(){
            public boolean dispatchKeyEvent(KeyEvent e) 
            {
                 if (e.getID()== KeyEvent.KEY_TYPED)
                 {
                     String key = "" + e.getKeyChar();
                     
                     if (key.equalsIgnoreCase("T"))
                     {
                         startButton.doClick();
                     } else if (key.equalsIgnoreCase("Y"))
                     {
                         pauseButton.doClick();
                     }else if (key.equalsIgnoreCase("Q"))
                     {
                         warningOneRedButton.doClick();
                     }else if (key.equalsIgnoreCase("A"))
                     {
                         onePointRedButton.doClick();
                     }else if (key.equalsIgnoreCase("Z"))
                     {
                         threePointRedButton.doClick();
                     }else if (key.equalsIgnoreCase("O"))
                     {
                         warningOneBlueButton.doClick();
                     }else if (key.equalsIgnoreCase("K"))
                     {
                         onePointBlueButton.doClick();
                     }else if (key.equalsIgnoreCase("M"))
                     {
                         threePointBlueButton.doClick();
                     }
                     
                  }
				return rootPaneCheckingEnabled;
              }
         };
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {

			  @Override
			  public void windowClosing(WindowEvent we)
			  { 
			    String ObjButtons[] = {"Si","No"};
			    int PromptResult = JOptionPane.showOptionDialog(null, 
			        "Lo stato del combattimento verrà perso, sei sicuro di vole uscire?", "Attenzione", 
			        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, 
			        ObjButtons,ObjButtons[1]);
			    if(PromptResult==0)
			    {
			        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);	
			        dispose();        
			    }
			  }
		});

	}
	
	public void endFightMessage(String message){
		
	    	FightStartedPanelImpl.timerFight.cancel();
	    	fightController.insertListaMatch(surnameFirstAthlete, surnameSecondAthlete, result);
	    	fightController.insertListaMatchFile();
		    JFrame frame = new JFrame("JOptionPane showMessageDialog example");			   
		    JOptionPane.showMessageDialog(frame,
		        message);	   
	        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);	

		    this.dispose();
	}
	
	@Override
	public void addObserver(FightController controller){
		
		this.fightController = controller;
	}
	private class LabelAdapter extends MouseAdapter {
		JWindow wa = new JWindow();
	    @Override
	    public void mouseEntered(MouseEvent e) {
	    	  JLabel istruzioni = new JLabel();
	    	  istruzioni.setText("<html>Istruzioni/Funzioni da tastiera:"
					+ "<br> Start->T &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Pause->Y"
			  		+ "<br>1 punto rosso->A	&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 	1 punto blu->K"
			  		+ "<br>3 punti rosso->z	&nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 3 punti blu->M"
			  		+ "<br>Ammonizione rosso->Q  &nbsp &nbsp &nbsp &nbsp  Ammonizione blu->O");
			  wa.add(istruzioni);
			  istruzioni.setFont(new Font("Arial", Font.BOLD, 30));
			  wa.setSize(100,100);
			  wa.setLocation(300,300);
			  wa.pack();
			  wa.setVisible(true);
	    }

	    @Override
	    public void mouseExited(MouseEvent e) {
	    	wa.setVisible(false);
	    }
	}
	
}