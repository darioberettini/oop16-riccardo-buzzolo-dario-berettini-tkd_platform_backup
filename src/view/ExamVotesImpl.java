package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.border.EmptyBorder;
import controller.ExamController;
import model.Athlete;
import model.Master;

public class ExamVotesImpl extends JFrame implements ExamVotes{

	private static final long serialVersionUID = -3350874684438626331L;
	
	private ExamController examController;
	private JPanel mainPanel = new JPanel();
	private JLabel athleteNameLabel;
	private JLabel[] mastersNamesLabel;
	private List<JSpinner> listText = new ArrayList<>();
	private JButton submitKicksButton = new JButton("Calci");
	private JButton submitFormButton = new JButton("Forma");
	private JButton submitFightButton = new JButton("Combattimento");
	private JButton submitButton = new JButton("Assegna");
	private JButton gobackButton = new JButton("Indietro");
	private JFrame guideFrame;
	public  ExamVotesImpl(List<Master> listMasters,Athlete athlete) {
		
		this.mastersNamesLabel = new JLabel[listMasters.size()];
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 500, 500);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
		
		
			athleteNameLabel = new JLabel(athlete.getName()+" "+athlete.getSurname());
			mainPanel.add(athleteNameLabel);
			
			submitKicksButton.setForeground(new Color(0, 0, 0));
			submitKicksButton.setBounds(80, 20, 100, 40);
			submitKicksButton.setFont(new Font("Arial", Font.BOLD, 14));
			// name.setBackground(new Color(255, 255, 255));
			mainPanel.add(submitKicksButton);
			
			submitFormButton.setForeground(new Color(0, 0, 0));
			submitFormButton.setBounds(200, 20, 100, 40);
			submitFormButton.setFont(new Font("Arial", Font.BOLD, 14));
			// name.setBackground(new Color(255, 255, 255));
			mainPanel.add(submitFormButton);
			
			submitFightButton.setForeground(new Color(0, 0, 0));
			submitFightButton.setBounds(320, 20, 150, 40);
			submitFightButton.setFont(new Font("Arial", Font.BOLD, 14));
			// name.setBackground(new Color(255, 255, 255));
			mainPanel.add(submitFightButton);	
			
			gobackButton.setForeground(new Color(0, 0, 0));
			gobackButton.setBounds(140, 410, 100, 40);
			gobackButton.setFont(new Font("Arial", Font.BOLD, 14));
			// name.setBackground(new Color(255, 255, 255));
			mainPanel.add(gobackButton);
	
			if(athlete.getVote(0)!=0){
				this.submitKicksButton.setEnabled(false);
			}
			if(athlete.getVote(1)!=0){
				this.submitFormButton.setEnabled(false);
			}
			if(athlete.getVote(2)!=0){
				this.submitFightButton.setEnabled(false);
			}
			
			submitKicksButton.addActionListener(e->{
				
				submitKicksButton.setEnabled(false);
				submitFightButton.setEnabled(false);
				submitFormButton.setEnabled(false);
				
				guideFrame = new JFrame();
				JPanel mainGuide = new JPanel();
				guideFrame.add(mainGuide);
				mainGuide.add(new JLabel(new ImageIcon(examController.openGuidaTecnica("calcio",athlete.getBelt().getValue()))));
				guideFrame.setVisible(true);
				guideFrame.pack();
				
				final JLabel kicks = new JLabel("Calci");
				kicks.setForeground(new Color(0, 0, 0));
				kicks.setBounds(230, 70, 100, 30);
				kicks.setFont(new Font("Arial", Font.BOLD, 15));
				// name.setBackground(new Color(255, 255, 255));
				mainPanel.add(kicks);
				
				for(int i=0; i<listMasters.size();i++){

					mastersNamesLabel[i] = new JLabel(listMasters.get(i).getSurname());
					SpinnerModel modelJS = new SpinnerNumberModel(5,1,10,1);
					JSpinner spinner = new JSpinner(modelJS);
					((DefaultEditor)spinner.getEditor()).getTextField().setEditable(false);
					listText.add(spinner);
				}
				
				int h = 110;
				for(JLabel label : mastersNamesLabel){	
					label.setForeground(new Color(0, 0, 0));
					label.setBounds(150, h, 100, 40);
					label.setFont(new Font("Arial", Font.BOLD, 15));
					// name.setBackground(new Color(255, 255, 255));
					mainPanel.add(label);
					h+= 50;
				}
				h = 110;
				for(JSpinner spinner : listText){
					spinner.setForeground(new Color(0, 0, 0));
					spinner.setBounds(270, h, 70, 40);
					spinner.setFont(new Font("Arial", Font.BOLD, 15));
					// name.setBackground(new Color(255, 255, 255));
					mainPanel.add(spinner);
					h += 50;
				}
				h = 110;
				
				submitButton.setForeground(new Color(0, 0, 0));
				submitButton.setBounds(260, 410, 100, 40);
				submitButton.setFont(new Font("Arial", Font.BOLD, 15));
				// name.setBackground(new Color(255, 255, 255));
				mainPanel.add(submitButton);
				
				submitButton.addActionListener(a->{
					guideFrame.dispose();
					Integer sum = 0;
					for(JSpinner spinner: listText){
						
						sum =sum + Integer.parseInt(spinner.getValue().toString());					
					}
					sum = sum / listMasters.size();
					athlete.setVote(0, sum);
					this.examController.startExamView();
					dispose();
				});
				SwingUtilities.updateComponentTreeUI(this);	
				this.repaint();
			});
			submitFormButton.addActionListener(e->{
				
				submitKicksButton.setEnabled(false);
				submitFightButton.setEnabled(false);
				submitFormButton.setEnabled(false);
				
				guideFrame = new JFrame();
				JPanel mainGuide = new JPanel();
				guideFrame.add(mainGuide);
				mainGuide.add(new JLabel(new ImageIcon(examController.openGuidaTecnica("forma",athlete.getBelt().getValue()))));
				guideFrame.setVisible(true);
				guideFrame.pack();
				
				final JLabel form = new JLabel("Forma");
				form.setForeground(new Color(0, 0, 0));
				form.setBounds(230, 70, 100, 30);
				form.setFont(new Font("Arial", Font.BOLD, 15));
				// name.setBackground(new Color(255, 255, 255));
				mainPanel.add(form);
				
				for(int i=0; i<listMasters.size();i++){

					mastersNamesLabel[i] = new JLabel(listMasters.get(i).getSurname());
					SpinnerModel modelJS = new SpinnerNumberModel(5,1,10,1);
					JSpinner spinner = new JSpinner(modelJS);
					((DefaultEditor)spinner.getEditor()).getTextField().setEditable(false);
					listText.add(spinner);
				}
				int h = 110;
				for(JLabel label : mastersNamesLabel){	
					label.setForeground(new Color(0, 0, 0));
					label.setBounds(150, h, 100, 40);
					label.setFont(new Font("Arial", Font.BOLD, 15));
					// name.setBackground(new Color(255, 255, 255));
					mainPanel.add(label);
					h+= 50;
				}
				h = 110;
				for(JSpinner spinner : listText){
					spinner.setForeground(new Color(0, 0, 0));
					spinner.setBounds(270, h, 70, 40);
					spinner.setFont(new Font("Arial", Font.BOLD, 15));
					// name.setBackground(new Color(255, 255, 255));
					mainPanel.add(spinner);
					h += 50;
				}
				h = 110;
				
				submitButton.setForeground(new Color(0, 0, 0));
				submitButton.setBounds(260, 410, 100, 40);
				submitButton.setFont(new Font("Arial", Font.BOLD, 15));
				// name.setBackground(new Color(255, 255, 255));
				mainPanel.add(submitButton);
				
				submitButton.addActionListener(a->{
					guideFrame.dispose();
					Integer sum = 0;
					for(JSpinner b: listText){
						
						sum +=  Integer.parseInt(b.getValue().toString());			
					}
					
					sum = sum / listMasters.size();
					athlete.setVote(1, sum);
					this.examController.startExamView();
					dispose();
				});
				SwingUtilities.updateComponentTreeUI(this);	
				
			});
			submitFightButton.addActionListener(e->{
				
				submitKicksButton.setEnabled(false);
				submitFightButton.setEnabled(false);
				submitFormButton.setEnabled(false);
				
				guideFrame = new JFrame();
				JPanel mainGuide = new JPanel();
				guideFrame.add(mainGuide);
				mainGuide.add(new JLabel(new ImageIcon(examController.openGuidaTecnica("combattimento",athlete.getBelt().getValue()))));
				guideFrame.setVisible(true);
				guideFrame.pack();
				
				final JLabel fight = new JLabel("Combattimento");
				fight.setForeground(new Color(0, 0, 0));
				fight.setBounds(200, 70, 200, 30);
				fight.setFont(new Font("Arial", Font.BOLD, 15));
				// name.setBackground(new Color(255, 255, 255));
				mainPanel.add(fight);
				
				JPanel focusPanel1 = new JPanel();
				mainPanel.add(focusPanel1);
				focusPanel1.setLayout(new GridLayout(listMasters.size(),2));
				for(int i=0; i<listMasters.size();i++){

					mastersNamesLabel[i] = new JLabel(listMasters.get(i).getSurname());
					SpinnerModel modelJS = new SpinnerNumberModel(5,1,10,1);
					JSpinner spinner = new JSpinner(modelJS);
					((DefaultEditor)spinner.getEditor()).getTextField().setEditable(false);
					listText.add(spinner);
				}
				int h = 110;
				for(JLabel label : mastersNamesLabel){	
					label.setForeground(new Color(0, 0, 0));
					label.setBounds(150, h, 100, 40);
					label.setFont(new Font("Arial", Font.BOLD, 15));
					// name.setBackground(new Color(255, 255, 255));
					mainPanel.add(label);
					h+= 50;
				}
				h = 110;
				for(JSpinner spinner : listText){
					spinner.setForeground(new Color(0, 0, 0));
					spinner.setBounds(270, h, 70, 40);
					spinner.setFont(new Font("Arial", Font.BOLD, 15));
					// name.setBackground(new Color(255, 255, 255));
					mainPanel.add(spinner);
					h += 50;
				}
				h = 110;
				
				submitButton.setForeground(new Color(0, 0, 0));
				submitButton.setBounds(260, 410, 100, 40);
				submitButton.setFont(new Font("Arial", Font.BOLD, 15));
				// name.setBackground(new Color(255, 255, 255));
				mainPanel.add(submitButton);
				
				submitButton.addActionListener(a->{
					guideFrame.dispose();
					Integer sum = 0;
					for(JSpinner b: listText){
						
						sum +=  Integer.parseInt(b.getValue().toString());					
					}
					sum = sum / listMasters.size();
					athlete.setVote(2, sum);
					this.examController.startExamView();
					dispose();
				});
				SwingUtilities.updateComponentTreeUI(this);	
			});
		
		gobackButton.addActionListener(e->{
			
			this.examController.startExamView();
			guideFrame.dispose();
			dispose();
			
		});
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
	
	public void addObserver(ExamController controller) {

		this.examController = controller;
	}

}
