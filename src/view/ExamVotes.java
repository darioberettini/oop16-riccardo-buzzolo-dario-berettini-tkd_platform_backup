package view;

import controller.ExamController;

public interface ExamVotes {

	public void addObserver(ExamController controller);
}
