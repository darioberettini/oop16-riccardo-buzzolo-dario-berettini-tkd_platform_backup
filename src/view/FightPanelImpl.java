package view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controller.FightController;

public class FightPanelImpl extends JFrame implements FightPanel{
	
	private static final long serialVersionUID = -4624979094418664594L;
	private FightController fightController;
	private final JButton startButton = new JButton("Start");
	private JButton printHistorical = new JButton("Storico");
	private JPanel mainPanel = new JPanel();
	private JLabel titlePanel = new JLabel("Inserimento Atleti");
	private JLabel firstAthlete = new JLabel("Atleta 1:");
	private JLabel secondAthlete = new JLabel("Atleta 2:");
	private JTextField surnameFirstAthlete = new JTextField("");
	private JTextField surnameSecondAthlete = new JTextField("");
	
	public FightPanelImpl(){
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 500, 450);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
				
		titlePanel.setForeground(new Color(0, 0, 0));
		titlePanel.setBounds(150, 30, 300, 40);
		titlePanel.setFont(new Font("Arial", Font.BOLD, 20));
		titlePanel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(titlePanel);
		
		firstAthlete.setForeground(new Color(0, 0, 0));
		firstAthlete.setBounds(40, 100, 100, 40);
		firstAthlete.setFont(new Font("Arial", Font.BOLD, 15));
		firstAthlete.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(firstAthlete);
		
		surnameFirstAthlete.setForeground(new Color(0, 0, 0));
		surnameFirstAthlete.setBounds(150, 100, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(surnameFirstAthlete);
		
		secondAthlete.setForeground(new Color(0, 0, 0));
		secondAthlete.setBounds(40, 180, 100, 40);
		secondAthlete.setFont(new Font("Arial", Font.BOLD, 15));
		secondAthlete.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(secondAthlete);
		
		surnameSecondAthlete.setForeground(new Color(0, 0, 0));
		surnameSecondAthlete.setBounds(150, 180, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(surnameSecondAthlete);
		
		printHistorical.setForeground(new Color(0, 0, 0));
		printHistorical.setFont(new Font("Arial", Font.BOLD, 13));
		printHistorical.setBounds(100, 300 , 120, 50);
		mainPanel.add(printHistorical);
		
		startButton.setForeground(new Color(0, 0, 0));
		startButton.setFont(new Font("Arial", Font.BOLD, 13));
		startButton.setBounds(280, 300 , 120, 50);
		mainPanel.add(startButton);
		
		this.setLocationRelativeTo(null);
		startButton.addActionListener(e -> {
			
			if(surnameFirstAthlete.getText().isEmpty() 
					|| surnameSecondAthlete.getText().isEmpty()){
			    JFrame frame = new JFrame("Errore");			   
			    JOptionPane.showMessageDialog(frame,
			      "Atleta/i non inserito/i.");
			}else if(!surnameFirstAthlete.getText().matches("[A-Za-z]+")
				|| !surnameSecondAthlete.getText().matches("[A-Za-z]+")){
				JFrame frame = new JFrame("Errore");			   
			    JOptionPane.showMessageDialog(frame,
			      "Inserire solo lettere.");
			}else{
				
				fightController.startFight(surnameFirstAthlete.getText(), surnameSecondAthlete.getText());
				setFocusable(true);
			}
		});
		
		printHistorical.addActionListener(e->{
			
			fightController.printAthletes();
			
		});
				
	}
	
	@Override
	public void addObserver(FightController controller) {
		this.fightController = controller;
		
	}
}
