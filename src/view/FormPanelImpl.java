package view;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import controller.FormController;
import model.Belt;

public class FormPanelImpl extends JFrame implements FormPanel{
	
	private static final long serialVersionUID = -5054917338852256007L;
	private FormController formControllerImpl;
	private JPanel mainPanel = new JPanel();
	private final JButton startButton = new JButton("Start");
	private final JButton printHistoricalButton = new JButton("Storico");
	private final JTextField nameText = new JTextField(20);
	private final JTextField surnameText = new JTextField(20);
	private final JLabel titlePanelLabel = new JLabel("Inserimento Studente");
	private final JLabel displayNameLabel = new JLabel("Nome :");
	private final JLabel displaySurnameLabel = new JLabel("Cognome :");
	private final JLabel displayDanLabel= new JLabel("Cintura :");
	private final JLabel displayFormLabel = new JLabel("Forma :");
	private String[] forms =  new String[]{"Taegeuk Il Jang – 1° Forma","Taegeuk I Jang – 2° Forma","Taegeuk Sam Jang – 3° Forma"
			,"Taegeuk Sa Jang – 4° Forma","Taegeuk Oh Jang – 5° Forma","Taegeuk Yuk Jang – 6° Forma",
			"Taegeuk Chil Jang – 7° Forma","Taegeuk Pal Jang – 8° Forma"};
	
	private final JComboBox<String> formListCB = new JComboBox<>(forms);
	private final JComboBox<Belt> beltListCB = new JComboBox<>(Belt.values());	
	public FormPanelImpl(){
		
		super("Forma");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 500, 600);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
	
		titlePanelLabel.setForeground(new Color(0, 0, 0));
		titlePanelLabel.setBounds(150, 30, 300, 40);
		titlePanelLabel.setFont(new Font("Arial", Font.BOLD, 20));
		titlePanelLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(titlePanelLabel);
		
		displayNameLabel.setForeground(new Color(0, 0, 0));
		displayNameLabel.setBounds(40, 100, 100, 40);
		displayNameLabel.setFont(new Font("Arial", Font.BOLD, 15));
		displayNameLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(displayNameLabel);
		
		nameText.setForeground(new Color(0, 0, 0));
		nameText.setBounds(150, 100, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(nameText);
		
		displaySurnameLabel.setForeground(new Color(0, 0, 0));
		displaySurnameLabel.setBounds(40, 180, 100, 40);
		displaySurnameLabel.setFont(new Font("Arial", Font.BOLD, 15));
		displaySurnameLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(displaySurnameLabel);
		
		surnameText.setForeground(new Color(0, 0, 0));
		surnameText.setBounds(150, 180, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(surnameText);
		
		displayFormLabel .setForeground(new Color(0, 0, 0));
		displayFormLabel .setBounds(40, 260, 100, 40);
		displayFormLabel .setFont(new Font("Arial", Font.BOLD, 15));
		displayFormLabel .setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(displayFormLabel );
		
		formListCB.setForeground(new Color(0, 0, 0));
		formListCB.setBounds(150, 260, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(formListCB);
		
		displayDanLabel.setForeground(new Color(0, 0, 0));
		displayDanLabel.setBounds(40, 340, 100, 40);
		displayDanLabel.setFont(new Font("Arial", Font.BOLD, 15));
		displayDanLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(displayDanLabel);
		
		beltListCB.setForeground(new Color(0, 0, 0));
		beltListCB.setBounds(150, 340, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(beltListCB);
		
		printHistoricalButton.setForeground(new Color(0, 0, 0));
		printHistoricalButton.setFont(new Font("Arial", Font.BOLD, 13));
		printHistoricalButton.setBounds(100, 440 , 120, 50);
		mainPanel.add(printHistoricalButton);
		
		startButton.setForeground(new Color(0, 0, 0));
		startButton.setFont(new Font("Arial", Font.BOLD, 13));
		startButton.setBounds(280, 440 , 120, 50);
		mainPanel.add(startButton);
		
		startButton.addActionListener(e -> {
			
			if(nameText.getText().isEmpty() 
					|| surnameText.getText().isEmpty()){
			    JFrame frame = new JFrame("Errore");			   
			    JOptionPane.showMessageDialog(frame,
			      "Nome o Cognome non inseriti.");
			}else if(!nameText.getText().matches("[A-Za-z]+")
					|| !surnameText.getText().matches("[A-Za-z]+")){
				JFrame frame = new JFrame("Errore");			   
			    JOptionPane.showMessageDialog(frame,
			      "Inserire solo lettere.");
			}else{
				System.out.println("Stampa cognome "+surnameText.getText()+"-");
				formControllerImpl.startForm(nameText.getText(), surnameText.getText(), (Belt) beltListCB.getSelectedItem(), formListCB.getSelectedItem().toString());
			}
		});
		
		printHistoricalButton.addActionListener(e->{
			
			formControllerImpl.printAthletes();			
		});
		this.setLocationRelativeTo(null);
	}
	
	public void addObserver(FormController controller) {
		
		this.formControllerImpl = controller;	
	}
}
