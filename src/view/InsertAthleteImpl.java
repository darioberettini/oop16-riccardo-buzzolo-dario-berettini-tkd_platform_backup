package view;

import java.awt.Color;
import java.awt.Font;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controller.ExamController;
import model.Belt;

public class InsertAthleteImpl extends JFrame implements InsertAthlete{
	
	private ExamController examController;
	private static final long serialVersionUID = 2244202524311031240L;
	private final JPanel mainPanel = new JPanel();
	private final JButton insertAthleteButton = new JButton("Inserisci");
	private final JButton undoButton = new JButton("Annulla");
	private final JTextField nameText = new JTextField(20);
	private final JTextField surnameText = new JTextField(20);
	private final JLabel titlePanelLabel = new JLabel("Inserimento Studente");
	private final JLabel nameLabel = new JLabel("Nome :");
	private final JLabel surnameLabel = new JLabel("Cognome :");
	private final JLabel danLabel= new JLabel("Cintura :");
	private final JComboBox<Belt> beltListCB = new JComboBox<>(Belt.values());	
	public InsertAthleteImpl(){
		
		super();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 500, 600);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
		
		titlePanelLabel.setForeground(new Color(0, 0, 0));
		titlePanelLabel.setBounds(150, 50, 300, 40);
		titlePanelLabel.setFont(new Font("Arial", Font.BOLD, 20));
		titlePanelLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(titlePanelLabel);
		
		nameLabel.setForeground(new Color(0, 0, 0));
		nameLabel.setBounds(40, 150, 100, 40);
		nameLabel.setFont(new Font("Arial", Font.BOLD, 15));
		nameLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(nameLabel);
		
		nameText.setForeground(new Color(0, 0, 0));
		nameText.setBounds(150, 150, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(nameText);
		
		surnameLabel.setForeground(new Color(0, 0, 0));
		surnameLabel.setBounds(40, 230, 100, 40);
		surnameLabel.setFont(new Font("Arial", Font.BOLD, 15));
		surnameLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(surnameLabel);
		
		surnameText.setForeground(new Color(0, 0, 0));
		surnameText.setBounds(150, 230, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(surnameText);
		
		danLabel.setForeground(new Color(0, 0, 0));
		danLabel.setBounds(40, 310, 100, 40);
		danLabel.setFont(new Font("Arial", Font.BOLD, 15));
		danLabel.setOpaque(true);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(danLabel);
		
		beltListCB.setForeground(new Color(0, 0, 0));
		beltListCB.setBounds(150, 310, 300, 40);
		// name.setBackground(new Color(255, 255, 255));
		mainPanel.add(beltListCB);
		
		undoButton.setForeground(new Color(0, 0, 0));
		undoButton.setFont(new Font("Arial", Font.BOLD, 13));
		undoButton.setBounds(100, 440 , 120, 50);
		mainPanel.add(undoButton);
		
		insertAthleteButton.setForeground(new Color(0, 0, 0));
		insertAthleteButton.setFont(new Font("Arial", Font.BOLD, 13));
		insertAthleteButton.setBounds(280, 440 , 120, 50);
		mainPanel.add(insertAthleteButton);
		
		insertAthleteButton.addActionListener(e->{
			if(nameText.getText().isEmpty()
					|| surnameText.getText().isEmpty()){
				JFrame frame = new JFrame("Errore");			   
			    JOptionPane.showMessageDialog(frame,
			      "Dati non inseriti!");				
			}else if(!nameText.getText().matches("[A-Za-z]+")
					|| !surnameText.getText().matches("[A-Za-z]+")){
				JFrame frame = new JFrame("Errore");			   
			    JOptionPane.showMessageDialog(frame,
			      "Valori non consentiti!");	
			}else{
				examController.insertAthlete(
						nameText.getText(),
						surnameText.getText(),
						(Belt) beltListCB.getSelectedItem());
				examController.updateAthlete();
				nameText.setText("");
				surnameText.setText("");
			}
		});
		undoButton.addActionListener(e->{
			
			dispose();
			
		});
		this.setLocationRelativeTo(null);
	}
	
	public void addObserver(ExamController controller) {
		this.examController = controller;
	}
}
