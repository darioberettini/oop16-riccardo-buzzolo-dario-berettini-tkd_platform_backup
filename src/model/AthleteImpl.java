package model;

import java.io.Serializable;

public class AthleteImpl implements Athlete, Serializable{
	
	private static final long serialVersionUID = 1L;
	private final String name;
	private final String surname;
	private final Belt athleteBelt;
	
	private final String formName;
	//Voto in ordine: Calci - Forma - Combattimento
	private final int[] votesArray;
	private int progress;
	private boolean promoted;
	private Double formScore;

	public AthleteImpl(String name, String surname, Double formScore){
		this.name = name;
		this.surname = surname;
		this.athleteBelt = null;
		this.votesArray = null;
		this.progress = 0;
		this.promoted=false;
		this.formName=null;
		this.formScore=formScore;
		
	}
	public AthleteImpl(String name, String surname, Belt belt) {
		this.name = name;
		this.surname = surname;
		this.athleteBelt = belt;
		this.votesArray = new int[3];
		this.progress = 0;
		this.promoted=false;
		this.formName=null;
		
	}
	
	public AthleteImpl(String name, String surname, Belt belt,String forma, Double formScore){
		this.name = name;
		this.surname = surname;
		this.athleteBelt = belt;
		this.votesArray = new int[3];
		this.progress = 0;
		this.promoted=false;
		this.formName = forma;
		this.formScore=formScore;
		
	}
	
	public String getName() {
		
		return name;
	}

	public String getSurname() {

		return surname;
	}

	public Belt getBelt() {

		return athleteBelt;
	}
	
	public void setVote(int index, int vote){
		this.votesArray[index]=vote;
		this.progress++;
	}
	
	public int getVote(int index){
		return this.votesArray[index];
	}
	
	public int getProgress(){
		return this.progress;
	}
	
	public String toString(){
		return name+" "+surname+" "+athleteBelt;
	}
	
	public void isPromoted(){
		if(this.progress == 3 && (this.votesArray[0]+this.votesArray[1]+this.votesArray[2]) >= 18){
			
			this.promoted=true;
		}else{
			
			this.promoted=false;
		}
	}
	
	public void setFormScore(Double vote){
		this.formScore = vote;
	}
	
	public double getFormScore(){
		return this.formScore;
	}
	
	public String getFormName(){
		return this.formName;
	}
	
	public boolean getPromoted(){
		return this.promoted;
	}
	public String toStringFormRes(){
		return this.name+" "+this.surname+" "+this.getFormScore();
	}
}
