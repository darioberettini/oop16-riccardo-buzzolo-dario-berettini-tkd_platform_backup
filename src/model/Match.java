package model;

import java.io.Serializable;

public interface Match  {
	
	/**
	 * 
	 * @return the info of the first Athletes
	 */
	public String getFirstAthleteSurname();
	/**
	 * 
	 * @return the info of the second Athletes
	 */
	public String getSecondAthleteSurname();
	/**
	 * 
	 * @return the info of the result of the Match
	 */
	public String getResult();
	
	public String toString();
}
