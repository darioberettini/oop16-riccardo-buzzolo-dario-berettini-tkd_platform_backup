package model;

public interface Master {
	/**
	 * 
	 * @return the name of the master
	 */
	String getName();
	/**
	 * 
	 * @return the surname of the master
	 */
	String getSurname();
	/**
	 * 
	 * @return the Dan of the master
	 */
	Integer getDan();
	/**
	 * 
	 * @return the info of the Master
	 */
	
	public String toString();
}
