package model;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;


import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class FightImpl implements Fight, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Match> listMatch;
	
	public FightImpl(){
		
		this.listMatch = new ArrayList<>();
	}
	
	public String[] getScoreBlue(Integer points){
		
		String[] s = new String[2];
		if(points<10){
			
			s[0]="/puntitaekwondo/"+ points+"_blue.png";
			return s;
			
		}if(points>=10){

			for(int i=10; i<100; i+=10){
				if(points-i <= 9){
				
					for(int j=0; j<10; j++){
						
						if(points-i-j == 0){
							s[0]="/puntitaekwondo/" + i/10 + "_blue.png";
							s[1]="/puntitaekwondo/" + j + "_blue.png";
							return s;
						}
					}
				}	
			}	
		}
		return null;		
	}
	
	public String[] getScoreRed(Integer points){
		
		
		String[] s = new String[2];
		if(points<10){
			
			s[0]="/puntitaekwondo/"+ points+"_red.png";
			return s;
			
		}if(points>=10){

			for(int i=10; i<50; i+=10){
				
				if(points-i <= 9){
					
					for(int j=0; j<10; j++){
						
						if(points-i-j == 0){
							s[0]="/puntitaekwondo/" + i/10 + "_red.png";
							s[1]="/puntitaekwondo/" + j + "_red.png";
							return s;
						}
					}
				}	
			}	
		}
		return null;		
	}
	
	public String getWarningRed(Integer warnings){
		
		if(warnings%2==0){
			return "/puntitaekwondo/red_warning.png";
		}else{
			return "/puntitaekwondo/yellow_warning.png";
		}	
		
	}
	
	public String getWarningBlue(Integer warnings){
		
		if(warnings%2==0){
			
			return "/puntitaekwondo/red_warning.png";
		}else{
			return "/puntitaekwondo/yellow_warning.png";
		}	
		
	}
	
	public void playSound(URL Sound){
		
		try{
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(Sound));
			clip.start();
			Thread.sleep(clip.getMicrosecondLength()/1000);
		}catch (Exception e) {		}		
	}
	
	public void insertListMatch(String firstAthleteSurname, String secondAthleteSurname, String result){
		
		this.listMatch.add(new MatchImpl(firstAthleteSurname,secondAthleteSurname,result));
	}
	
	public void setListMatch(ArrayList<Match> lista){
		
		this.listMatch=lista;
	}
	
	public ArrayList<Match> getListMatch(){
		
		return this.listMatch;
	}
	
	public void insertListMatchFile() {
		
		BufferedWriter writer = null;
		  try{

		      writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream("resource/storici/ListaMatch.dat")));
		      for(Match m : listMatch){
		      	writer.write(m.toString()+"\n");
				}

		      writer.flush();
		      writer.close();

		  }    
		  catch(IOException e) {
			  e.printStackTrace();
		  }
		  
		/*
	
	
		FileWriter fw = null;
		BufferedWriter bw = null;
		
		try{
		
			fw = new FileWriter("resource/storici/ListaMatch.dat");
			bw = new BufferedWriter(fw);
			for(Match m : listMatch){
				bw.write(m.toString()+"\n");
				bw.flush();
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {

			try {
				if (bw != null){
					
					bw.close();
				}
				if (fw != null){
					fw.close();
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		/*try {
			
			
			FileOutputStream stream = new FileOutputStream("resource/storici/ListaMatch.dat");
			
			ObjectOutputStream osStream = new ObjectOutputStream(stream);

			osStream.writeObject(listMatch);
			
			osStream.flush();

			osStream.close();

		} catch (Exception e) {

			System.out.println("I/O errore"+e);
		}*/
	}
/*
 * You can't use File, since this file does not exist independently on the file system.
 *  Instead you need getResourceAsStream()
 */
	public ArrayList<Match> getListMatchFile() {
	
		BufferedReader	br1 = null;
		if(listMatch.isEmpty()){
		try {
   		
   		br1 = new BufferedReader(new InputStreamReader(FightImpl.class.getResourceAsStream("/storici/ListaMatch.dat")));		 
     		String sCurrentLine;	
   		while ((sCurrentLine = br1.readLine()) != null) {
   		
   				String[] arr = sCurrentLine.split(" ");  
   				String firstAthleteN = arr[0];
   				String secondAthleteN = arr[1];
   				String result="";
   				
   				for(int i=2; i<arr.length; i++){
   					
   					result = result+" "+arr[i];
   				}
   				listMatch.add(new MatchImpl(firstAthleteN, secondAthleteN, result));	
   			}
   			return listMatch;
   			
   		} catch (Exception e) {
   			e.printStackTrace();
   		}  finally {
   
   			try {
   
   				if (br1 != null)
   					br1.close();
   
   
   			} catch (IOException ex) {
   
   				ex.printStackTrace();
   
   			}
   		}	
		}	
		else
			return listMatch;

		
		ArrayList<Match> arrayEmpty = new ArrayList<Match>();
		return arrayEmpty;
	}
}
