package model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FormImpl implements Form {
	
	private ArrayList<Athlete> listAthletes;
	public FormImpl(){
		
		this.listAthletes = new ArrayList<>();
	}
		
	public String[] getScoreRed(int points){
		
		String[] s = new String[3];
		
		if(points < 100){			
			
			int unit = points-((points/10)*10);
			int dozen = points - unit;
			s[0]="/puntitaekwondo/" + 0 + "_red.png";
			s[1]="/puntitaekwondo/" + dozen/10 + "_red.png";
			s[2]="/puntitaekwondo/" + unit + "_red.png";
			return s;						
		}
		
		return null;		
	}
	public List<Athlete> getListAthletesForm() {
	
		return listAthletes;
	}
	
	public ArrayList<String> printAthletes(){
	
		ArrayList<String> printedList = new ArrayList<>();
		for(Athlete athlete : listAthletes){
			
			printedList.add(athlete.getName()+" "+athlete.getSurname());
		}
		
		return printedList;
	}
	
	public void addAhtletesForm(Athlete athlete) {
		
		this.listAthletes.add(athlete);
	}
	
	public void addListAthletesFormFile() {
		
		BufferedWriter writer = null;
		  try{

		      writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream("resource/storici/ListaAtletiForma.dat")));
		      for(Athlete a : listAthletes){
		      	writer.write(a.toStringFormRes()+"\n");
				}

		      writer.flush();
		      writer.close();

		  }    
		  catch(IOException e) {
		      e.printStackTrace();
		}
		/*try {

			FileOutputStream stream = new FileOutputStream("resource/storici/ListaAtletiForma.dat");
			ObjectOutputStream osStream = new ObjectOutputStream(stream);

			osStream.writeObject(listAthletes);
			osStream.flush();
			osStream.close();

		} catch (Exception e) {

			System.out.println("I/O errore");
		}*/
	}
	
	public ArrayList<Athlete> getListAthletesFormFile() {
		BufferedReader	br1 = null;
		if(listAthletes.isEmpty()){
		try {
   		
   		br1 = new BufferedReader(new InputStreamReader(FightImpl.class.getResourceAsStream("/storici/ListaMatch.dat")));		 
     		String sCurrentLine;	
   		while ((sCurrentLine = br1.readLine()) != null) {
   		
   				String[] arr = sCurrentLine.split(" ");  
   				String name = arr[0];
   				String surname = arr[1];
   				String result=arr[2];
   				
   				listAthletes.add(new AthleteImpl(name, surname, Double.parseDouble(result)));	
   			}
   			return listAthletes;
   			
   		} catch (Exception e) {
   			e.printStackTrace();
   		}  finally {
   
   			try {
   
   				if (br1 != null)
   					br1.close();
   
   
   			} catch (IOException ex) {
   
   				ex.printStackTrace();
   
   			}
   		}	
		}	
		else
			return listAthletes;
		
		ArrayList<Athlete> arrayEmpty = new ArrayList<Athlete>();
		return arrayEmpty;
	}
}
