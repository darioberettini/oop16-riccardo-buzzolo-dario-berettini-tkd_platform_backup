package model;

import java.io.Serializable;

public class MatchImpl implements Match, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5254877310621642833L;
	private String atleta1;
	private String atleta2;
	private String risultato;
	
	public MatchImpl(String firstAthleteSurname, String secondAthleteSurname, String result){
		
		this.atleta1=firstAthleteSurname;
		this.atleta2=secondAthleteSurname;
		this.risultato=result;
	}

	public String getFirstAthleteSurname() {
		
		return atleta1;
	}
	
	public String getSecondAthleteSurname() {
		
		return atleta2;
	}
	
	public String getResult(){
		
		return risultato;
	}
	public String toString(){
		
		return getFirstAthleteSurname()+" "+getSecondAthleteSurname()+" "+getResult();
	}
}
